import asyncio
import copy
import json
import time

from aiohttp import ClientSession

from . import __version__
from .config import Config


async def fetch(url, data, session):
    try:
        async with session.post(url, data=data) as response:
            response = await response.read()
            # todo: json loads here or after all requests are sent?
            response = json.loads(response)
            response.update(data=data)
            return response
    except Exception as ex:
        # todo: print? log? what?
        return {'success': False, 'data': data}


async def run(url=None, data=None, headers=None):
    tasks = []

    # Fetch all responses within one Client session,
    # keep connection alive for all requests.
    async with ClientSession(headers=headers) as session:
        for i, d in enumerate(data):
            print('START', i)
            task = asyncio.ensure_future(fetch(url.format(i), d, session))
            tasks.append(task)

        responses = await asyncio.gather(*tasks)
        # you now have all response bodies in this variable
        print(responses)


class Client(object):
    _headers = {'User-Agent': 'hl-client/{}'.format(__version__), }

    def __init__(self, config):
        self.config = Config(config, config_type='client')

    @property
    def url(self):
        if 'url' in self.config:
            return self.config['url']
        host = self.config['host']
        path = self.config['path']

        if host[-1] == '/':
            host = host[:-1]
        if path[0] != '/':
            path = '/{}'.format(path)

        return '{host}{path}'.format(host=host, path=path)

    @property
    def headers(self):
        headers = copy.copy(self._headers)
        if 'token' in self.config:
            headers.update({'Authorization': self.config['token']})
        return self._headers

    def send(self, data=None, *args, **kwargs):
        start = time.time()
        self._send_rest(data, *args, **kwargs)
        end = time.time()
        print("Total time: {}".format(end - start))
        # requests.post(self.url, data={'metric': 'io.cpu.percent'}, headers=self.headers)

    def _send_rest(self, data, *args, **kwargs):
        loop = asyncio.get_event_loop()
        future = asyncio.ensure_future(run(url=self.url, data=data, headers=self.headers))
        loop.run_until_complete(future)

    def _send_websocket(self, data, *args, **kwargs):
        raise NotImplementedError

    def _send_graphql(self, data, *args, **kwargs):
        raise NotImplementedError
