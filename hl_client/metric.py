import datetime
import uuid

#  metric, log, alert, event  hrn
class MetricType(object):
    def __init__(self, metric_type):
        if isinstance(metric_type, str):
            self._str = metric_type
            self._arr = tuple(metric_type.split('.'))
        elif isinstance(metric_type, (list, tuple)):
            self._str = '.'.join(metric_type)
            self._arr = tuple(metric_type)

    def __str__(self):
        return self._str

    # def __call__(self, *args, **kwargs):
    #     return self._str


class Metric(object):
    id: uuid.UUID = None
    device: uuid.UUID = None
    metric: MetricType = None
    date_time_uuid: uuid.UUID = None
    date_time: datetime.datetime = None
    value: float = None

    reporting_device: uuid.UUID = None

