from typing import Union

import yaml


class Config(dict):
    config_type=None
    _config = {}

    def __init__(self, config: Union[str, dict]=None, config_type: str =None):
        self.config_type = config_type or self.config_type
        if isinstance(config, (str,)):
            with open(config) as conf_file:
                config = yaml.safe_load(conf_file)
        self._config = config.get(self.config_type, config)
        super().__init__(self._config)

    def load(self, config, config_type=None):
        self.config_type = config_type or self.config_type


        # if config_type and config_type in config:
        #     config = config[config_type]
        self._config = config.get(self.config_type, config)
